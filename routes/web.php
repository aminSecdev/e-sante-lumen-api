<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
   $get = "http://10.193.70.129/api/individus";
    //return "get all data is'$get'";
    return view('index');
});
$router->group(['prefix' => 'api'], function () use ($router) {
    // Matches "/api/register
    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');
 

  

    $router->get('/patients', 'patientcontroller@index');
    $router->post('/patient', 'patientcontroller@create');
    $router->get('/patient/{id}', 'patientcontroller@show');
    $router->put('/patient/{id}', 'patientcontroller@update');
    $router->delete('/patient/{id}', 'patientcontroller@destroy');
    
 });
